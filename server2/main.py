# [START gae_python37_app]
from flask import *
import csv
import time
import json


sids_cache = {}
prdns_cache = {}

def get_sids(dir):
  if dir not in sids_cache:
    with open('data_sids_{}.csv'.format(dir)) as file:
      sids = csv.reader(file.read())
      sids_cache[dir] = sids[1:]
      return sids[1:]
  else:
    return sids_cache[dir]

def get_prediction(sid):
  if sid not in prdns_cache.keys():
    with open('data_prdn_{}.csv'.format(sid)) as file:
      prdns = csv.reader(file.read())
  else:
    prdns = prdns_cache[sid]
  nowmillitime = time.time()
  for prdn in prdns:
    diff = prdn[0] - prdn
    if diff < 0:
      diff = diff * -1
    

def get_predictions(dir):
  sids = get_sids(dir)
  for sid in sids:
    get_prediction(sid)

app = Flask(__name__)

with open('./pred_db.json', 'r') as file:
  preds = json.load(file)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/hamster')
def hamster():
  return render_template('hamster.html')

def _predict(sid, time):
  print('Predict {} {}'.format(sid, time))
  if sid in preds.keys():
    with open(preds[sid], 'r') as file:
      data = list(csv.reader(file))
      pred = 'Unknown'
      for i in range(0, len(data)/1000, 1000):
        if data[0] in range(time - 60000, time + 60000, 1):
          pred = data[1]
      return render_template('pred.html', pred = pred  , sid = sid)
    return render_template('error.html', code='2', short='Data Processing', long='Opening, reading, parsing the data, rendering template failed. SID exists. ({})'.format(type(err)))
  return render_template('error.html', code = '1', short = 'Nonexisting SID', long = 'The requested session ID does not exist in the database.')

@app.route('/hamster/sid/<sid>/<time>')
def predict(sid, time):
  _predict(sid, time)

@app.errorhandler(404)
def error404(error):
  return render_template('error.html', code='404', short='Nonexisting Locator', long='Route not found.')

@app.errorhandler(500)
def error500(error):
  return render_template('error.html', code='500', short='Internal Error', long='Error encountered while processing request.')

@app.errorhandler(400)
def error400(error):
  return render_template('error.html', code='400', short='Forbidden', long='Unable to access forbidden route.')


if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080, debug=False, use_evalex=False)
# [END gae_python37_app]
