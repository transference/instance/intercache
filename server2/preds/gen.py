def gen(path, rate, limit = 6, bounce = True):
    with open(path, 'w') as file:
        c = 0
        tc = 0
        for i in range(86400):
            print(i, tc, c, rate)
            for j in range(1000):
                file.write('{},{}\n'.format(tc, c))
                tc += 1
            c += rate
            if c >= limit or c <= 0:
                rate *= -1

if __name__ == '__main__':
    print('Gen')
    gen(input('Path '), int(input('Rate ')), int(input('Limit ')))