# [START gae_python37_app]
from flask import *
import csv
import time
import json
from datetime import time
import lib


# sids_cache = {}
# prdns_cache = {}
#
# def get_sids(dir):
#   if dir not in sids_cache:
#     with open('data_sids_{}.csv'.format(dir)) as file:
#       sids = csv.reader(file.read())
#       sids_cache[dir] = sids[1:]
#       return sids[1:]
#   else:
#     return sids_cache[dir]
#
# def get_prediction(sid):
#   if sid not in prdns_cache.keys():
#     with open('data_prdn_{}.csv'.format(sid)) as file:
#       prdns = csv.reader(file.read())
#   else:
#     prdns = prdns_cache[sid]
#   nowmillitime = time.time()
#   for prdn in prdns:
#     diff = prdn[0] - prdn
#     if diff < 0:
#       diff = diff * -1
#
# def get_predictions(dir):
#   sids = get_sids(dir)
#   for sid in sids:
#     get_prediction(sid)


app = Flask(__name__)

with open('./pred_db.json', 'r') as file:
  preds = json.load(file)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/hamster')
def hamster():
  return render_template('hamster.html')

@app.route('/hamster/raw/<sid>/<time>/<dist>')
def predict_raw(sid, time, dist):
  return json.dumps(lib._predict(sid, time, dist))
# <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox={{ raw_lat-0.005 }}%2C{{ raw_lon }}%2C-79.27030026912689%2C43.732499358244155&amp;layer=mapnik&amp;marker=43.731255086677066%2C-79.27265256643295" style="border: 0px;">Map is not displayed. Allow iframe to display map.</iframe>


@app.route('/hamster/sid/<sid>/<time>/<dist>')
def predict(sid, time, dist):
  result = lib._predict(sid, time, dist)
  if result == 1:
    return render_template('error.html', code='500-1', short='Unsupported SID',
                           long='SID is not supported.')
  return render_template('pred.html', sid=sid, time=time, dist=dist, wait_time=result[1], now_dist=result[2],
                         debug=result['debug'], raw_time=result['raw_time'], raw_lon=result[4], raw_lat=result[5],
                         raw_dist=result[6], vehicle=result['vehicle'])

@app.route('/hamster/min/<sid>/<time>/<dist>')
def predict_min(sid, time, dist):
  result = lib._predict(sid, time, dist)
  if result == 1:
    return render_template('error.html', code='500-1', short='Unsupported SID',
                           long='SID is not supported.')
  return render_template('pred_min.html', sid=sid, time=time, dist=dist, wait_time=result[1], now_dist=result[2],
                         debug=result['debug'], raw_time=result['raw_time'], raw_lon=result[4], raw_lat=result[5],
                         raw_dist=result[6], vehicle=result['vehicle'])

@app.route('/hamster/scrollable/<int:n_preds>')
def predict_scrollable(n_preds):
  urls = []
  for i in range(n_preds):
    # Pr(ediction) ID
    prid = [request.args.get('sid{}'.format(i)), request.args.get('time{}'.format(i)), request.args.get('dist{}'.format(i))]
    print(prid)
    urls.append('/hamster/min/{}/{}/{}'.format(*prid))
  del i, prid
  return render_template('scrollable.html', urls = urls)

@app.errorhandler(404)
def error404(error):
  return render_template('error.html', code='404', short='Route not found.',
                         long='There were no responses for URL "{}".'.format(request.url))

@app.errorhandler(500)
def error500(error):
  return render_template('error.html', code='500', short='Internal Error',
                         long='Error encountered while processing request.')

@app.errorhandler(403)
def error403(error):
  return render_template('error.html', code='403', short='Forbidden',
                         long='Unable to access forbidden route.')

@app.errorhandler(410)
def error410(error):
  return render_template('error.html', code='410', short='Gone',
                         long='Unable to access gone route.')


if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080, debug=False, use_evalex=False)
# [END gae_python37_app]
